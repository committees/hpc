/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2306                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p
    {
        solver                    GAMG;
        tolerance                 1e-7;
        relTol                    0.1;
        smoother                  GaussSeidel;
        minIter                   1;
    }

    pFinal
    {
        $p;
        relTol                    0;
    }

    Phi
    {
        $p;
    }

    PhiFinal
    {
        $pFinal;
    }
    "(U|nuTilda)"
    {
        solver           PBiCGStab;
        preconditioner   DILU;
        tolerance        1e-10;
        relTol           0.1;
    }

    "(U|nuTilda)Final"
    {
        $U;
        tolerance        1e-09;
        relTol           0.0;
    }
}

potentialFlow
{
    nNonOrthogonalCorrectors      10;
}

PIMPLE
{
    nOuterCorrectors              5;
    nCorrectors                   2;
    nNonOrthogonalCorrectors      1;
}

relaxationFactors
{
    fields
    {
        p           0.95;
        pFinal      1;
    }
    equations
    {
      U             0.9;
      nuTilda       0.9;
      UFinal        1;
      nuTildaFinal  1;
    }
}

cache
{
    grad(U);
}

// ************************************************************************* //
