/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  3.0.x                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvOptions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "$FOAM_CASE/constant/initialConditions"

turbine1
{
    type            axialFlowTurbineALSource;
    active          off;

    axialFlowTurbineALSourceCoeffs
    {
        fieldNames          (U);
        selectionMode       cellSet; // cellSet || points || cellZone
        cellSet             actuationDisk1;
        origin              (400 1140 102);
        axis                $axisInitial;
        verticalDirection   (0 0 1);
        freeStreamVelocity  $UInitial;
        tipSpeedRatio       7.55;
        rotorRadius         63.0;

        dynamicStall
        {
            active          off;
            dynamicStallModel LeishmanBeddoes;
        }

        endEffects
        {
            active          off;
            endEffectsModel Shen;
            GlauertCoeffs
            {
                tipEffects  on;
                rootEffects on;
            }
            ShenCoeffs
            {
                tipEffects  on;
                rootEffects on;
                c1          0.125;
                c2          21;
            }
        }

        blades
        {
            blade1
            {
                writePerf   true;
                writeElementPerf true;
                nElements   17;
                elementProfiles
                (
			#include "NREL5MW_Blade/NREL5MW_17_elementProfiles"
                );
                elementData
                ( 
                	#include "NREL5MW_Blade/NREL5MW_17_elementData"
		);
                collectivePitch 0.0;
            }
            blade2
            {
                $blade1;
                writePerf   false;
                writeElementPerf false;
                azimuthalOffset 120.0;
            }
            blade3
            {
                $blade2;
                azimuthalOffset 240.0;
            }
        }
/*
        tower
        {
            includeInTotalDrag  false; // default is false
            nElements   2; //6;
            elementProfiles (cylinder);
            elementData
            ( // axial distance (turbine axis), height, diameter
                (10.0 -90.0 4.50)
                (10.0  0.00 3.50)
            );
        }

        hub
        {
            nElements   2;
            elementProfiles (cylinder);
            elementData
            ( // axial distance, height, diameter
                (0  1.5 3)
                (0 -1.5 3)
            );
        }
*/
        profileData
        {
            // profile names : DU99W405LM DU99W350LM DU97W300LM DU91W2250LM DU93W210LM NACA64618 circular050 circular035 cylinder

            DU99W405LM
            {
                tableType   singleRe; // singleRe (default) || multiRe
             //   Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/DU40_A17"
                );
            }

            DU99W350LM
            {
                tableType   singleRe; // singleRe (default) || multiRe
             //   Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/DU35_A17"
                );
            }

            DU97W300LM
            {
                tableType   singleRe; // singleRe (default) || multiRe
             //   Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/DU30_A17"
                );
            }

            DU91W2250LM
            {
                tableType   singleRe; // singleRe (default) || multiRe
             //   Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/DU25_A17"
		);
            }

            DU93W210LM
            {
                tableType   singleRe; // singleRe (default) || multiRe
               // Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/DU21_A17"
                );
            }

            NACA64618
            {
                tableType   singleRe; // singleRe (default) || multiRe
               // Re          1e6; // For tableType = singleRe
                data // For tableType = singleRe
                (   // alpha C_l C_d
                    #include "NREL5MW_foil/NACA64_A17"
                );
            }

            circular050
            {
                data ((-180 0 0.50)(180 0 0.50));
            }

            circular035
            {
                data ((-180 0 0.35)(180 0 0.35));
            }

            cylinder
            {
                //data ((-180 0 1.1)(180 0 1.1));
                data ((-180 0 0.0)(180 0 0.0)); //no tower nor hub 
            }
        }
    }
}

turbine2
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk2;
        origin              (1350 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    } 
}

turbine3
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk3;
        origin              (2300 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    }
}

turbine4
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk4;
        origin              (3250 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    }
}

turbine5
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk5;
        origin              (4200 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    }
}

turbine6
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk6;
        origin              (5150 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    }
}

turbine7
{
    $turbine1;

    axialFlowTurbineALSourceCoeffs
    {
        cellSet             actuationDisk7;
        origin              (6100 1140 102);
//        freeStreamVelocity  (4.75 0 0);
    }
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
