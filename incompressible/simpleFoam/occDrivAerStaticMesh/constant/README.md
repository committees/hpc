## Obtaining mesh files
The polyMesh files may be obtained from the following links:
Coarse - <a rel="coarse" href="https://www.icloud.com/iclouddrive/0bagTnqDUje5-MIYbXIThw2hA#polyMesh%5F50M">polyMesh_50M.tar.gz</a>
Medium - <a rel="medium" href="https://www.icloud.com/iclouddrive/025KzMJMz1KsCxlqoPgbrdDBg#polyMesh%5F110M">polyMesh_110M.tar.gz</a>
Fine - <a rel="fine" href="https://www.icloud.com/iclouddrive/0ac4GxgJFpqdpgVH4iHtS7KvA#polyMesh%5F236M">polyMesh_236M.tar.gz</a>
* There is no requirement to download all three sets of mesh files

After downloading, copy the tar file to the `constant/` folder, and untar with `tar -zxvf polyMesh_50M.tar.gz`.

