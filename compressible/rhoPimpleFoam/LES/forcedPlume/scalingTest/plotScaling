#!/bin/sh

set -e


PERSIST=""  # "-p" for the plot image to persist on the screen
IMAGE_FORMAT="png"


error()
{
  echo ERROR: $1
  exit 0
}

# user param set 
# dataFile="example.dat"

dataFile="$1"

# logscale=""
xlogscale="set logscale x"
ylogscale="set logscale y"

[ -f $dataFile ] || error "input data file $dataFile not found"
[ "$#" -ge 1 ] || error "no data file provided as argument"

# set data columns in input file 
nCells=5
nCores=6
executionTime=7
firstLine=2  # skip header line (used as reference for speedup)


# Execution Time 
gnuplot $PERSIST << eof

# unset xtics
# set ytics 2e3  scale 0.5 font ",6"
# unset title
# set format y "%.2e" 
# set border 2
# set key left top
set nokey

# set title "Performance: Scaling" font ",12"
# set xtics nomirror
# set xtics scale 1 font ",6"  

set xlabel "number of tasks" font ",12"
set ylabel "execution time (s)" font ",12" offset 1,0,0

plot '$dataFile' using $nCores:$executionTime with linespoint lw 2 pt 11 

# print figure
set terminal png
set output "executionTime.$IMAGE_FORMAT"
replot

eof

# Speedup 
gnuplot $PERSIST << eof

# unset xtics
# set ytics 2e3  scale 0.5 font ",6"
# unset title
# set format y "%.2e" 
# set border 2
# set key left top
set nokey

# set title "Performance: Scaling" font ",12"
# set xtics nomirror
# set xtics (10 100 1000 10000) 

set xlabel "number of tasks" font ",12"
set ytics  nomirror tc lt 1
set ylabel "Speedup" font ",12" offset 1,0,0
set y2tics nomirror tc lt 2
set y2label 'Efficiency' tc lt 2
set y2range [0:1.5]

$xlogscale
$ylogscale

# Functions for scaling metrics
speedup(t_ref,w_ref,t,w) = t_ref*w/(t*w_ref)
eff(t_ref,w_ref,Np_ref,t,w,Np) = (t_ref/t)/(Np/Np_ref)
# eff(t_ref,w_ref,Np_ref,t,w,Np) = (t_ref/t)/(Np/Np_ref)
ideal(Np_ref,Np)=Np/Np_ref

refTime=system("awk 'FNR == $firstLine {print \$$executionTime}' $dataFile")
refNproc=system("awk 'FNR == $firstLine {print \$$nCores}' $dataFile")
refWork=system("awk 'FNR == $firstLine {print \$$nCells}' $dataFile")
print("reference time")
print(refTime)
print("reference number of processor")
print(refNproc)
print("reference work load (nCells)")
print(refWork)

plot '$dataFile' u $nCores:(speedup(refTime,refWork,\$$executionTime,\$$nCells)) with linespoint lw 2 pt 11, \
'$dataFile' u $nCores:(ideal(refNproc,\$$nCores))  with lines lt -1 dt ".-"  axes x1y1, \
'$dataFile' u $nCores:(eff(refTime,refNproc,\$$executionTime,\$$nCores)) with linespoint lw 2 axes x1y2
# '$dataFile' u $nCores:(eff(refTime,refWork,refNproc,\$$executionTime,\$$nCells,\$$nCores)) with linespoint lw 2 axes x1y2

# ideal(refNproc,\$1) with lines
# plot '$dataFile' using 1:(eff(refTime,refNproc,\$2,\$1)) with linespoint lw 2 pt 11 

# try labeling nCells/core
# plot '$dataFile' using 1:(speedup(ref,\$2)):(sprintf("(%d, %d)", \$1, (f(ref,\$2)) )) with linespoint pt 11 , '' with labels
# offset char 1,1 

# print figure
set terminal png
set output "speedup.$IMAGE_FORMAT"
replot
eof

